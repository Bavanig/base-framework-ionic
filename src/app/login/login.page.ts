import { Component, OnInit } from '@angular/core';
import { MenuController, LoadingController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../services/common.service';
import { environment } from '../../environments/environment.prod';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm/ngx';
declare var device: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public appVersion = '1.1.4';
  public eyeOn = false;
  public type = 'password';
  public loginForm: FormGroup;
  constructor(public menu: MenuController, private formBuilder: FormBuilder, public loadingController: LoadingController, public commonService: CommonService, public router: Router, public storage: Storage, public fcm: FCM, public alertController: AlertController) {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.storage.get('userDetails').then((val) => {
      console.log('userDetails =', val);
      if (val !== undefined && val !== null) {
        this.router.navigate(['/dashboard']);
      }
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.menu.enable(false);
  }

  async login() {
    const loading = await this.loadingController.create({
      message: 'Loading...'
    });
    await loading.present();
    const actionURL = "domain/login/" + this.loginForm.value.userName + "/" + this.loginForm.value.password + '?targetDomainKey=' + environment.DOMAIN_KEY;
    await this.commonService.getAllCall(actionURL)
      .subscribe(res => {
        console.log(res);
        if (res) {
          const actionUrl = 'sql/exec/multi/' + environment.API_TOKEN;
          const data = {
            "queries": [
              { "type": "SELECT", "query": "SELECT * FROM " + environment.DRIVER_MASTER + " WHERE email='" + this.loginForm.value.userName + "'" },
            ]
          }
          this.commonService.postCall(actionUrl, JSON.stringify(data))
            .subscribe(result => {
              res['driverObj'] = result.results[0].records[0];
              res['driverObj']['driverimg'] = environment.fileUrl + environment.API_TOKEN + '/' + res['driverObj']['driverimg'];
              environment.API_KEY = res.apiKey;
              environment.API_TOKEN = res.token;
              environment.DOMAIN_KEY = res.domainKey;
              this.storage.set('userDetails', res);
              /////////////////// FCM //////////////////////
              this.fcm.subscribeToTopic('drivermodule');
              this.fcm.getToken().then(token => {
                const path = "fcm/register/" + environment.API_TOKEN + "/" + res['driverObj'].email + "/" + device.model + "/" + device.version + "/" + token;
                this.commonService.getAllCall(path)
                  .subscribe(res => {
                    this.commonService.presentToast('Device ID registered successfully');
    
                  })

              });

              this.fcm.onNotification().subscribe(data => {
                this.presentAlertConfirm('You got new trip!')
              });

              /////////////////// FCM //////////////////////
              this.router.navigate(['/dashboard']);
            })

        } else {
          this.commonService.presentToast('Username/Password Invalid')
        }

        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }


  async presentAlertConfirm(status) {
    const alert = await this.alertController.create({
      header: 'New Trip Alert',
      message: status,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.commonService.presentToast('Your option have been saved.');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.router.navigate(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }
}
