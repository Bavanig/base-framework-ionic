import { Component } from '@angular/core';

import { Platform, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Network } from '@ionic-native/network/ngx';
import { AlertController } from '@ionic/angular';
import { CommonService } from './services/common.service';
import { Storage } from '@ionic/storage';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { Insomnia } from '@ionic-native/insomnia/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Dashboard',
      url: '/dashboard',
      icon: 'home'
    },
    {
      title: 'Templates',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'Report',
      url: '/documents',
      icon: 'document'
    },
 
    
   

    {
      title: 'Profile',
      url: '/settings',
      icon: 'person'
    },
     {
      title: 'Help & Support',
      url: '/help',
      icon: 'help-circle'
    }, 
    {
      title: 'Logout',
      url: '/',
      icon: 'log-out'
    }
  ];
  public alert: any;
  public userDetails: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private network: Network,
    public alertController: AlertController,
    public commonService: CommonService,
    public storage: Storage,
    public router: Router,
    public loadingController: LoadingController,
    private insomnia: Insomnia
  ) {
    this.initializeApp();
    this.router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationStart) {
          // Show loading indicator
          if (event.url === "/dashboard") {
            this.storage.get('userDetails').then((val) => {
              this.userDetails = val ? val : {}
            })
          }
      }

      if (event instanceof NavigationEnd) {
          // Hide loading indicator
      }

      if (event instanceof NavigationError) {
          // Hide loading indicator

          // Present error to user
          console.log(event.error);
      }
  });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.storage.get('userDetails').then((val) => {
        this.userDetails = val ? val : undefined
      })
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.statusBar.backgroundColorByHexString('#4db26d');
      this.insomnia.keepAwake()
        .then(
          () => console.log('success'),
          () => console.log('error')
        );

      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        console.log('network was disconnected :-(');
        this.presentAlertConfirm();
      });

      // watch network for a connection
      let connectSubscription = this.network.onConnect().subscribe(() => {
        console.log('network connected!');
        if (this.alert) {
          this.alert.dismiss();
        }
      });
      // this.fcm.subscribeToTopic('Application');
      // this.fcm.getToken().then(token => {
      //   alert(JSON.stringify(token))
      // });

      // this.fcm.onNotification().subscribe(data => {
      //   if (data.wasTapped) {
      //     console.log("Received in background");
      //   } else {
      //     console.log("Received in foreground");
      //   };
      // });
    });
  }
  async presentAlertConfirm() {
    this.alert = await this.alertController.create({
      header: 'Attention!',
      backdropDismiss: false,
      message: 'No <strong>network</strong> connection!!!',
      buttons: [
        {
          text: 'Exit',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            navigator['app'].exitApp();
          }
        }
      ]
    });

    await this.alert.present();
  }
  async logout() {
    const loading = await this.loadingController.create({
      message: 'Loading...'
    });
    await loading.present();
    this.router.navigate(['/login']);
    this.storage.set('userDetails', undefined);

    loading.dismiss();
  }
}
