(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["list-list-module"],{

/***/ "./src/app/list/list.module.ts":
/*!*************************************!*\
  !*** ./src/app/list/list.module.ts ***!
  \*************************************/
/*! exports provided: ListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPageModule", function() { return ListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list.page */ "./src/app/list/list.page.ts");







var ListPageModule = /** @class */ (function () {
    function ListPageModule() {
    }
    ListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _list_page__WEBPACK_IMPORTED_MODULE_6__["ListPage"]
                    }
                ])
            ],
            declarations: [_list_page__WEBPACK_IMPORTED_MODULE_6__["ListPage"]]
        })
    ], ListPageModule);
    return ListPageModule;
}());



/***/ }),

/***/ "./src/app/list/list.page.html":
/*!*************************************!*\
  !*** ./src/app/list/list.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"headers\">\n\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title slot=\"start\">\n      Templates\n\n    </ion-title>\n\n \n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  \n</ion-content>"

/***/ }),

/***/ "./src/app/list/list.page.scss":
/*!*************************************!*\
  !*** ./src/app/list/list.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Blue Flat Button\n==================================================*/\n.btn-xlarge {\n  border-radius: 16px;\n  position: relative;\n  vertical-align: center;\n  color: white;\n  text-align: center;\n  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);\n  background: #666666;\n  border: 0;\n  border-bottom: 3px solid #4db26d;\n  cursor: pointer;\n  box-shadow: inset 0 -3px #4db26d; }\n.btn-xlarge:active {\n  top: 2px;\n  outline: none;\n  box-shadow: none; }\n.btn-xlarge:hover {\n  background: #45E1E8; }\n.toolbar-background {\n  background: #666666 !important; }\n.list-group {\n  border-radius: 0; }\n.list-group .list-group-item {\n  background-color: transparent;\n  overflow: hidden;\n  border: 0;\n  border-radius: 0;\n  padding: 0 16px; }\n.list-group .list-group-item .row-picture,\n.list-group .list-group-item .row-action-primary {\n  float: left;\n  display: inline-block;\n  padding-right: 16px;\n  padding-top: 8px; }\n.list-group .list-group-item .row-picture img,\n.list-group .list-group-item .row-action-primary img,\n.list-group .list-group-item .row-picture i,\n.list-group .list-group-item .row-action-primary i,\n.list-group .list-group-item .row-picture label,\n.list-group .list-group-item .row-action-primary label {\n  display: block;\n  width: 56px;\n  height: 56px; }\n.list-group .list-group-item .row-picture img,\n.list-group .list-group-item .row-action-primary img {\n  background: rgba(0, 0, 0, 0.1);\n  padding: 1px; }\n.list-group .list-group-item .row-picture img.circle,\n.list-group .list-group-item .row-action-primary img.circle {\n  border-radius: 100%; }\n.list-group .list-group-item .row-picture i,\n.list-group .list-group-item .row-action-primary i {\n  background: rgba(0, 0, 0, 0.25);\n  border-radius: 100%;\n  text-align: center;\n  line-height: 56px;\n  font-size: 20px;\n  color: white; }\n.list-group .list-group-item .row-picture label,\n.list-group .list-group-item .row-action-primary label {\n  margin-left: 7px;\n  margin-right: -7px;\n  margin-top: 5px;\n  margin-bottom: -5px; }\n.list-group .list-group-item .row-content {\n  display: inline-block;\n  width: calc(100% - 92px);\n  min-height: 66px; }\n.list-group .list-group-item .row-content .action-secondary {\n  position: absolute;\n  right: 16px;\n  top: 16px; }\n.list-group .list-group-item .row-content .action-secondary i {\n  font-size: 20px;\n  color: rgba(0, 0, 0, 0.25);\n  cursor: pointer; }\n.list-group .list-group-item .row-content .action-secondary ~ * {\n  max-width: calc(100% - 30px); }\n.list-group .list-group-item .row-content .least-content {\n  position: absolute;\n  right: 16px;\n  top: 0px;\n  color: rgba(0, 0, 0, 0.54);\n  font-size: 14px; }\n.list-group .list-group-item .list-group-item-heading {\n  color: rgba(0, 0, 0, 0.77);\n  font-size: 20px;\n  line-height: 29px; }\n.list-group .list-group-separator {\n  clear: both;\n  overflow: hidden;\n  margin-top: 10px;\n  margin-bottom: 10px; }\n.list-group .list-group-separator:before {\n  content: \"\";\n  width: calc(100% - 90px);\n  border-bottom: 1px solid rgba(0, 0, 0, 0.1);\n  float: right; }\n.bg-profile {\n  background-color: #3498DB !important;\n  height: 150px;\n  z-index: 1; }\n.bg-bottom {\n  height: 100px;\n  margin-left: 30px; }\n.img-profile {\n  display: inline-block !important;\n  background-color: #fff;\n  border-radius: 6px;\n  margin-top: -50%;\n  padding: 1px;\n  vertical-align: bottom;\n  border: 2px solid #fff;\n  box-sizing: border-box;\n  color: #fff;\n  z-index: 2; }\n.row-float {\n  margin-top: -40px; }\n.explore a {\n  color: green;\n  font-size: 13px;\n  font-weight: 600; }\n.twitter a {\n  color: #4099FF; }\n.img-box {\n  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);\n  border-radius: 2px;\n  border: 0; }\nion-tab-button.tab-selected.tab-has-label.tab-has-icon.tab-layout-icon-top.ion-activatable.hydrated {\n  color: #8c8c8c; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2JhdmFuaS9EZXNrdG9wL3dvcmtzcGFjZS9kcml2ZXJhcHAvc3JjL2FwcC9saXN0L2xpc3QucGFnZS5zY3NzIiwic3JjL2FwcC9saXN0L2xpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO21EQ0FtRDtBREVuRDtFQUNJLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsMENBQTBDO0VBQzFDLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsZ0NBQWdDO0VBQ2hDLGVBQWU7RUFDZixnQ0FBZ0MsRUFBQTtBQUVsQztFQUNFLFFBQVE7RUFDUixhQUFhO0VBRWIsZ0JBQWdCLEVBQUE7QUFFbEI7RUFDRSxtQkFBbUIsRUFBQTtBQUVyQjtFQUNFLDhCQUE4QixFQUFBO0FBRWxDO0VBQWEsZ0JBQWdCLEVBQUE7QUFDN0I7RUFBOEIsNkJBQTZCO0VBQUMsZ0JBQWdCO0VBQUMsU0FBUztFQUFDLGdCQUFnQjtFQUFDLGVBQWUsRUFBQTtBQUN2SDs7RUFDa0QsV0FBVztFQUFDLHFCQUFxQjtFQUFDLG1CQUFtQjtFQUFDLGdCQUFnQixFQUFBO0FBQ3hIOzs7Ozs7RUFLd0QsY0FBYztFQUFDLFdBQVc7RUFBQyxZQUFZLEVBQUE7QUFDL0Y7O0VBQ3NELDhCQUE4QjtFQUFDLFlBQVksRUFBQTtBQUNqRzs7RUFDNkQsbUJBQW1CLEVBQUE7QUFDaEY7O0VBQ29ELCtCQUErQjtFQUFDLG1CQUFtQjtFQUFDLGtCQUFrQjtFQUFDLGlCQUFpQjtFQUFDLGVBQWU7RUFBQyxZQUFZLEVBQUE7QUFDeks7O0VBQ3dELGdCQUFnQjtFQUFDLGtCQUFrQjtFQUFDLGVBQWU7RUFBQyxtQkFBbUIsRUFBQTtBQUMvSDtFQUEyQyxxQkFBcUI7RUFBQyx3QkFBd0I7RUFBQyxnQkFBZ0IsRUFBQTtBQUMxRztFQUE2RCxrQkFBa0I7RUFBQyxXQUFXO0VBQUMsU0FBUyxFQUFBO0FBQ3JHO0VBQStELGVBQWU7RUFBQywwQkFBMEI7RUFBQyxlQUFlLEVBQUE7QUFDekg7RUFBaUUsNEJBQTRCLEVBQUE7QUFDN0Y7RUFBMEQsa0JBQWtCO0VBQUMsV0FBVztFQUFDLFFBQVE7RUFBQywwQkFBMEI7RUFBQyxlQUFlLEVBQUE7QUFDNUk7RUFBdUQsMEJBQTBCO0VBQUMsZUFBZTtFQUFDLGlCQUFpQixFQUFBO0FBQ25IO0VBQW1DLFdBQVc7RUFBQyxnQkFBZ0I7RUFBQyxnQkFBZ0I7RUFBQyxtQkFBbUIsRUFBQTtBQUNwRztFQUEwQyxXQUFXO0VBQUMsd0JBQXdCO0VBQUMsMkNBQTJDO0VBQUMsWUFBWSxFQUFBO0FBRXZJO0VBQVksb0NBQW9DO0VBQUMsYUFBYTtFQUFDLFVBQVUsRUFBQTtBQUN6RTtFQUFXLGFBQWE7RUFBQyxpQkFBaUIsRUFBQTtBQUMxQztFQUFhLGdDQUFnQztFQUFDLHNCQUFzQjtFQUFDLGtCQUFrQjtFQUFDLGdCQUFnQjtFQUFDLFlBQVk7RUFBQyxzQkFBc0I7RUFBQyxzQkFBc0I7RUFBNkIsc0JBQXNCO0VBQUMsV0FBVztFQUFDLFVBQVUsRUFBQTtBQUM3TztFQUFXLGlCQUFpQixFQUFBO0FBQzVCO0VBQVksWUFBWTtFQUFFLGVBQWU7RUFBQyxnQkFBZ0IsRUFBQTtBQUMxRDtFQUFZLGNBQWEsRUFBQTtBQUN6QjtFQUFTLHdFQUErRDtFQUFDLGtCQUFrQjtFQUFDLFNBQVMsRUFBQTtBQUNyRztFQUNFLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xpc3QvbGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qIEJsdWUgRmxhdCBCdXR0b25cbj09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Ki9cbi5idG4teGxhcmdle1xuICAgIGJvcmRlci1yYWRpdXM6IDE2cHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHZlcnRpY2FsLWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB0ZXh0LXNoYWRvdzogMCAxcHggMnB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYmFja2dyb3VuZDogIzY2NjY2NjtcbiAgICBib3JkZXI6IDA7XG4gICAgYm9yZGVyLWJvdHRvbTogM3B4IHNvbGlkICM0ZGIyNmQ7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJveC1zaGFkb3c6IGluc2V0IDAgLTNweCAjNGRiMjZkO1xufVxuICAuYnRuLXhsYXJnZTphY3RpdmUge1xuICAgIHRvcDogMnB4O1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gIH1cbiAgLmJ0bi14bGFyZ2U6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6ICM0NUUxRTg7XG4gIH1cbiAgLnRvb2xiYXItYmFja2dyb3VuZCB7XG4gICAgYmFja2dyb3VuZDogIzY2NjY2NiAhaW1wb3J0YW50O1xufVxuLmxpc3QtZ3JvdXAge2JvcmRlci1yYWRpdXM6IDA7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSB7YmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7b3ZlcmZsb3c6IGhpZGRlbjtib3JkZXI6IDA7Ym9yZGVyLXJhZGl1czogMDtwYWRkaW5nOiAwIDE2cHg7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUsXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkge2Zsb2F0OiBsZWZ0O2Rpc3BsYXk6IGlubGluZS1ibG9jaztwYWRkaW5nLXJpZ2h0OiAxNnB4O3BhZGRpbmctdG9wOiA4cHg7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaW1nLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGltZyxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGksXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgaSxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGxhYmVsLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGxhYmVsIHtkaXNwbGF5OiBibG9jazt3aWR0aDogNTZweDtoZWlnaHQ6IDU2cHg7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaW1nLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGltZyB7YmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjEpO3BhZGRpbmc6IDFweDt9XG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctcGljdHVyZSBpbWcuY2lyY2xlLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGltZy5jaXJjbGUge2JvcmRlci1yYWRpdXM6IDEwMCU7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaSxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBpIHtiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMjUpO2JvcmRlci1yYWRpdXM6IDEwMCU7dGV4dC1hbGlnbjogY2VudGVyO2xpbmUtaGVpZ2h0OiA1NnB4O2ZvbnQtc2l6ZTogMjBweDtjb2xvcjogd2hpdGU7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgbGFiZWwsXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgbGFiZWwge21hcmdpbi1sZWZ0OiA3cHg7bWFyZ2luLXJpZ2h0OiAtN3B4O21hcmdpbi10b3A6IDVweDttYXJnaW4tYm90dG9tOiAtNXB4O31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1jb250ZW50IHtkaXNwbGF5OiBpbmxpbmUtYmxvY2s7d2lkdGg6IGNhbGMoMTAwJSAtIDkycHgpO21pbi1oZWlnaHQ6IDY2cHg7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWNvbnRlbnQgLmFjdGlvbi1zZWNvbmRhcnkge3Bvc2l0aW9uOiBhYnNvbHV0ZTtyaWdodDogMTZweDt0b3A6IDE2cHg7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWNvbnRlbnQgLmFjdGlvbi1zZWNvbmRhcnkgaSB7Zm9udC1zaXplOiAyMHB4O2NvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMjUpO2N1cnNvcjogcG9pbnRlcjt9XG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctY29udGVudCAuYWN0aW9uLXNlY29uZGFyeSB+ICoge21heC13aWR0aDogY2FsYygxMDAlIC0gMzBweCk7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWNvbnRlbnQgLmxlYXN0LWNvbnRlbnQge3Bvc2l0aW9uOiBhYnNvbHV0ZTtyaWdodDogMTZweDt0b3A6IDBweDtjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjU0KTtmb250LXNpemU6IDE0cHg7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAubGlzdC1ncm91cC1pdGVtLWhlYWRpbmcge2NvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNzcpO2ZvbnQtc2l6ZTogMjBweDtsaW5lLWhlaWdodDogMjlweDt9XG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1zZXBhcmF0b3Ige2NsZWFyOiBib3RoO292ZXJmbG93OiBoaWRkZW47bWFyZ2luLXRvcDogMTBweDttYXJnaW4tYm90dG9tOiAxMHB4O31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLXNlcGFyYXRvcjpiZWZvcmUge2NvbnRlbnQ6IFwiXCI7d2lkdGg6IGNhbGMoMTAwJSAtIDkwcHgpO2JvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMSk7ZmxvYXQ6IHJpZ2h0O31cblxuLmJnLXByb2ZpbGV7YmFja2dyb3VuZC1jb2xvcjogIzM0OThEQiAhaW1wb3J0YW50O2hlaWdodDogMTUwcHg7ei1pbmRleDogMTt9XG4uYmctYm90dG9te2hlaWdodDogMTAwcHg7bWFyZ2luLWxlZnQ6IDMwcHg7fVxuLmltZy1wcm9maWxle2Rpc3BsYXk6IGlubGluZS1ibG9jayAhaW1wb3J0YW50O2JhY2tncm91bmQtY29sb3I6ICNmZmY7Ym9yZGVyLXJhZGl1czogNnB4O21hcmdpbi10b3A6IC01MCU7cGFkZGluZzogMXB4O3ZlcnRpY2FsLWFsaWduOiBib3R0b207Ym9yZGVyOiAycHggc29saWQgI2ZmZjstbW96LWJveC1zaXppbmc6IGJvcmRlci1ib3g7Ym94LXNpemluZzogYm9yZGVyLWJveDtjb2xvcjogI2ZmZjt6LWluZGV4OiAyO31cbi5yb3ctZmxvYXR7bWFyZ2luLXRvcDogLTQwcHg7fVxuLmV4cGxvcmUgYSB7Y29sb3I6IGdyZWVuOyBmb250LXNpemU6IDEzcHg7Zm9udC13ZWlnaHQ6IDYwMH1cbi50d2l0dGVyIGEge2NvbG9yOiM0MDk5RkZ9XG4uaW1nLWJveHtib3gtc2hhZG93OiAwIDNweCA2cHggcmdiYSgwLDAsMCwuMTYpLDAgM3B4IDZweCByZ2JhKDAsMCwwLC4yMyk7Ym9yZGVyLXJhZGl1czogMnB4O2JvcmRlcjogMDt9XG5pb24tdGFiLWJ1dHRvbi50YWItc2VsZWN0ZWQudGFiLWhhcy1sYWJlbC50YWItaGFzLWljb24udGFiLWxheW91dC1pY29uLXRvcC5pb24tYWN0aXZhdGFibGUuaHlkcmF0ZWQge1xuICBjb2xvcjogIzhjOGM4Yztcbn0iLCIvKiBCbHVlIEZsYXQgQnV0dG9uXG49PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovXG4uYnRuLXhsYXJnZSB7XG4gIGJvcmRlci1yYWRpdXM6IDE2cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdmVydGljYWwtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtc2hhZG93OiAwIDFweCAycHggcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgYmFja2dyb3VuZDogIzY2NjY2NjtcbiAgYm9yZGVyOiAwO1xuICBib3JkZXItYm90dG9tOiAzcHggc29saWQgIzRkYjI2ZDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBib3gtc2hhZG93OiBpbnNldCAwIC0zcHggIzRkYjI2ZDsgfVxuXG4uYnRuLXhsYXJnZTphY3RpdmUge1xuICB0b3A6IDJweDtcbiAgb3V0bGluZTogbm9uZTtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xuICBib3gtc2hhZG93OiBub25lOyB9XG5cbi5idG4teGxhcmdlOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzQ1RTFFODsgfVxuXG4udG9vbGJhci1iYWNrZ3JvdW5kIHtcbiAgYmFja2dyb3VuZDogIzY2NjY2NiAhaW1wb3J0YW50OyB9XG5cbi5saXN0LWdyb3VwIHtcbiAgYm9yZGVyLXJhZGl1czogMDsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgcGFkZGluZzogMCAxNnB4OyB9XG5cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZy1yaWdodDogMTZweDtcbiAgcGFkZGluZy10b3A6IDhweDsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctcGljdHVyZSBpbWcsXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgaW1nLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaSxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBpLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgbGFiZWwsXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDU2cHg7XG4gIGhlaWdodDogNTZweDsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctcGljdHVyZSBpbWcsXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgaW1nIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBwYWRkaW5nOiAxcHg7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaW1nLmNpcmNsZSxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBpbWcuY2lyY2xlIHtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctcGljdHVyZSBpLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGkge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGxpbmUtaGVpZ2h0OiA1NnB4O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiB3aGl0ZTsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctcGljdHVyZSBsYWJlbCxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBsYWJlbCB7XG4gIG1hcmdpbi1sZWZ0OiA3cHg7XG4gIG1hcmdpbi1yaWdodDogLTdweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtNXB4OyB9XG5cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1jb250ZW50IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogY2FsYygxMDAlIC0gOTJweCk7XG4gIG1pbi1oZWlnaHQ6IDY2cHg7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWNvbnRlbnQgLmFjdGlvbi1zZWNvbmRhcnkge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxNnB4O1xuICB0b3A6IDE2cHg7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWNvbnRlbnQgLmFjdGlvbi1zZWNvbmRhcnkgaSB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gIGN1cnNvcjogcG9pbnRlcjsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctY29udGVudCAuYWN0aW9uLXNlY29uZGFyeSB+ICoge1xuICBtYXgtd2lkdGg6IGNhbGMoMTAwJSAtIDMwcHgpOyB9XG5cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1jb250ZW50IC5sZWFzdC1jb250ZW50IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTZweDtcbiAgdG9wOiAwcHg7XG4gIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNTQpO1xuICBmb250LXNpemU6IDE0cHg7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAubGlzdC1ncm91cC1pdGVtLWhlYWRpbmcge1xuICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjc3KTtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjlweDsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1zZXBhcmF0b3Ige1xuICBjbGVhcjogYm90aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1zZXBhcmF0b3I6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDkwcHgpO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBmbG9hdDogcmlnaHQ7IH1cblxuLmJnLXByb2ZpbGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OERCICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTUwcHg7XG4gIHotaW5kZXg6IDE7IH1cblxuLmJnLWJvdHRvbSB7XG4gIGhlaWdodDogMTAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAzMHB4OyB9XG5cbi5pbWctcHJvZmlsZSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jayAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiA2cHg7XG4gIG1hcmdpbi10b3A6IC01MCU7XG4gIHBhZGRpbmc6IDFweDtcbiAgdmVydGljYWwtYWxpZ246IGJvdHRvbTtcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZjtcbiAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBjb2xvcjogI2ZmZjtcbiAgei1pbmRleDogMjsgfVxuXG4ucm93LWZsb2F0IHtcbiAgbWFyZ2luLXRvcDogLTQwcHg7IH1cblxuLmV4cGxvcmUgYSB7XG4gIGNvbG9yOiBncmVlbjtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNjAwOyB9XG5cbi50d2l0dGVyIGEge1xuICBjb2xvcjogIzQwOTlGRjsgfVxuXG4uaW1nLWJveCB7XG4gIGJveC1zaGFkb3c6IDAgM3B4IDZweCByZ2JhKDAsIDAsIDAsIDAuMTYpLCAwIDNweCA2cHggcmdiYSgwLCAwLCAwLCAwLjIzKTtcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xuICBib3JkZXI6IDA7IH1cblxuaW9uLXRhYi1idXR0b24udGFiLXNlbGVjdGVkLnRhYi1oYXMtbGFiZWwudGFiLWhhcy1pY29uLnRhYi1sYXlvdXQtaWNvbi10b3AuaW9uLWFjdGl2YXRhYmxlLmh5ZHJhdGVkIHtcbiAgY29sb3I6ICM4YzhjOGM7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/list/list.page.ts":
/*!***********************************!*\
  !*** ./src/app/list/list.page.ts ***!
  \***********************************/
/*! exports provided: ListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPage", function() { return ListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_device_motion_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/device-motion/ngx */ "./node_modules/@ionic-native/device-motion/ngx/index.js");










var ListPage = /** @class */ (function () {
    function ListPage(menu, navCtrl, platform, commonService, router, loadingController, storage, formBuilder, datePipe, geolocation, deviceMotion) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.commonService = commonService;
        this.router = router;
        this.loadingController = loadingController;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.datePipe = datePipe;
        this.geolocation = geolocation;
        this.deviceMotion = deviceMotion;
        this.openMenu = false;
        this.showLiveUpdatePopup = false;
        this.menu.enable(true);
    }
    ListPage.prototype.searchFilter = function () {
        this.togglePopupMenu();
    };
    ListPage.prototype.togglePopupMenu = function () {
        if (!this.showLiveUpdatePopup) {
            return this.openMenu = !this.openMenu;
        }
    };
    ListPage.prototype.navigateTo = function (page) {
        this.router.navigate(['/' + page]);
    };
    ListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.page.html */ "./src/app/list/list.page.html"),
            styles: [__webpack_require__(/*! ./list.page.scss */ "./src/app/list/list.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"], _ionic_native_device_motion_ngx__WEBPACK_IMPORTED_MODULE_9__["DeviceMotion"]])
    ], ListPage);
    return ListPage;
}());



/***/ })

}]);
//# sourceMappingURL=list-list-module.js.map