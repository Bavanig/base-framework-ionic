(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-settings-module"],{

/***/ "./src/app/settings/settings.module.ts":
/*!*********************************************!*\
  !*** ./src/app/settings/settings.module.ts ***!
  \*********************************************/
/*! exports provided: SettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./settings.page */ "./src/app/settings/settings.page.ts");







var routes = [
    {
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]
    }
];
var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]]
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());



/***/ }),

/***/ "./src/app/settings/settings.page.html":
/*!*********************************************!*\
  !*** ./src/app/settings/settings.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"headers\">\n\n    <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"start\">\n        <ion-menu-button></ion-menu-button>\n      </ion-buttons>\n      <ion-title slot=\"start\">\n        Profile Details\n  \n      </ion-title>\n  \n   \n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content>\n    \n  </ion-content>"

/***/ }),

/***/ "./src/app/settings/settings.page.scss":
/*!*********************************************!*\
  !*** ./src/app/settings/settings.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bs-calltoaction {\n  position: relative;\n  width: auto;\n  padding: 15px 25px;\n  border: 1px solid black;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border-radius: 5px; }\n\n.bs-calltoaction > .row {\n  display: table;\n  width: calc(100% + 30px); }\n\n.bs-calltoaction > .row > [class^=\"col-\"],\n.bs-calltoaction > .row > [class*=\" col-\"] {\n  float: none;\n  display: table-cell;\n  vertical-align: middle; }\n\n.cta-contents {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.cta-title {\n  margin: 0 auto 15px;\n  padding: 0; }\n\n.cta-desc {\n  padding: 0; }\n\n.cta-desc p:last-child {\n  margin-bottom: 0; }\n\n.cta-button {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n@media (max-width: 991px) {\n  .bs-calltoaction > .row {\n    display: block;\n    width: auto; }\n  .bs-calltoaction > .row > [class^=\"col-\"],\n  .bs-calltoaction > .row > [class*=\" col-\"] {\n    float: none;\n    display: block;\n    vertical-align: middle;\n    position: relative; }\n  .cta-contents {\n    text-align: center; } }\n\n.bs-calltoaction.bs-calltoaction-default {\n  color: #333;\n  background-color: #fff;\n  border-color: #ccc; }\n\n.bs-calltoaction.bs-calltoaction-primary {\n  color: #fff;\n  background-color: #337ab7;\n  border-color: #2e6da4; }\n\n.bs-calltoaction.bs-calltoaction-info {\n  color: #fff;\n  background-color: #5bc0de;\n  border-color: #46b8da; }\n\n.bs-calltoaction.bs-calltoaction-success {\n  color: #fff;\n  background-color: #5cb85c;\n  border-color: #4cae4c; }\n\n.bs-calltoaction.bs-calltoaction-warning {\n  color: #fff;\n  background-color: #f0ad4e;\n  border-color: #eea236; }\n\n.bs-calltoaction.bs-calltoaction-danger {\n  color: #fff;\n  background-color: #d9534f;\n  border-color: #d43f3a; }\n\n.bs-calltoaction.bs-calltoaction-primary .cta-button .btn,\n.bs-calltoaction.bs-calltoaction-info .cta-button .btn,\n.bs-calltoaction.bs-calltoaction-success .cta-button .btn,\n.bs-calltoaction.bs-calltoaction-warning .cta-button .btn,\n.bs-calltoaction.bs-calltoaction-danger .cta-button .btn {\n  border-color: #fff; }\n\n.fb-profile img.fb-image-lg {\n  z-index: 0;\n  width: 100%;\n  margin-bottom: 10px; }\n\n.fb-image-profile {\n  margin: -90px 10px 0px 50px;\n  z-index: 9;\n  width: 20%; }\n\n@media (max-width: 768px) {\n  .fb-profile-text > h1 {\n    font-weight: 700;\n    font-size: 16px; }\n  .fb-image-profile {\n    margin: -45px 10px 0px 25px;\n    z-index: 9;\n    width: 20%; } }\n\n.card {\n  background-color: rgba(214, 224, 226, 0.2);\n  border-top-width: 0;\n  border-bottom-width: 2px;\n  border-radius: 3px;\n  box-shadow: none;\n  box-sizing: border-box; }\n\n.card .card-heading {\n  padding: 0 20px;\n  margin: 0; }\n\n.card .card-heading.simple {\n  font-size: 20px;\n  font-weight: 300;\n  color: #777;\n  border-bottom: 1px solid #e5e5e5; }\n\n.card .card-heading.image img {\n  display: inline-block;\n  width: 46px;\n  height: 46px;\n  margin-right: 15px;\n  vertical-align: top;\n  border: 0;\n  border-radius: 50%; }\n\n.card .card-heading.image .card-heading-header {\n  display: inline-block;\n  vertical-align: top; }\n\n.card .card-heading.image .card-heading-header h3 {\n  margin: 0;\n  font-size: 14px;\n  line-height: 16px;\n  color: #262626; }\n\n.card .card-heading.image .card-heading-header span {\n  font-size: 12px;\n  color: #999999; }\n\n.card .card-body {\n  padding: 0 20px;\n  margin-top: 20px; }\n\n.card .card-media {\n  padding: 0 20px;\n  margin: 0 -14px; }\n\n.card .card-media img {\n  max-width: 100%;\n  max-height: 100%; }\n\n.card .card-actions {\n  min-height: 30px;\n  padding: 0 20px 20px 20px;\n  margin: 20px 0 0 0; }\n\n.card .card-comments {\n  padding: 20px;\n  margin: 0;\n  background-color: #f8f8f8; }\n\n.card .card-comments .comments-collapse-toggle {\n  padding: 0;\n  margin: 0 20px 12px 20px; }\n\n.card .card-comments .comments-collapse-toggle a,\n.card .card-comments .comments-collapse-toggle span {\n  padding-right: 5px;\n  overflow: hidden;\n  font-size: 12px;\n  color: #999;\n  text-overflow: ellipsis;\n  white-space: nowrap; }\n\n.card-comments .media-heading {\n  font-size: 13px;\n  font-weight: bold; }\n\n.card.people {\n  position: relative;\n  display: inline-block;\n  width: 170px;\n  height: 300px;\n  padding-top: 0;\n  margin-left: 20px;\n  overflow: hidden;\n  vertical-align: top; }\n\n.card.people:first-child {\n  margin-left: 0; }\n\n.card.people .card-top {\n  position: absolute;\n  top: 0;\n  left: 0;\n  display: inline-block;\n  width: 170px;\n  height: 150px;\n  background-color: #ffffff; }\n\n.card.people .card-top.green {\n  background-color: #53a93f; }\n\n.card.people .card-top.blue {\n  background-color: #427fed; }\n\n.card.people .card-info {\n  position: absolute;\n  top: 150px;\n  display: inline-block;\n  width: 100%;\n  height: 101px;\n  overflow: hidden;\n  background: #ffffff;\n  box-sizing: border-box; }\n\n.card.people .card-info .title {\n  display: block;\n  margin: 8px 14px 0 14px;\n  overflow: hidden;\n  font-size: 16px;\n  font-weight: bold;\n  line-height: 18px;\n  color: #404040; }\n\n.card.people .card-info .desc {\n  display: block;\n  margin: 8px 14px 0 14px;\n  overflow: hidden;\n  font-size: 12px;\n  line-height: 16px;\n  color: #737373;\n  text-overflow: ellipsis; }\n\n.card.people .card-bottom {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  display: inline-block;\n  width: 100%;\n  padding: 10px 20px;\n  line-height: 29px;\n  text-align: center;\n  box-sizing: border-box; }\n\n.card.hovercard {\n  position: relative;\n  padding-top: 0;\n  overflow: hidden;\n  text-align: center;\n  background-color: rgba(214, 224, 226, 0.2); }\n\n.card.hovercard .cardheader {\n  background: url(\"https://api.boodskap.io/files/public/download/471233c1-7933-493f-9852-4c5ac6aa1352\");\n  background-size: cover;\n  height: 200px; }\n\n.card.hovercard .avatar {\n  position: relative;\n  top: -50px;\n  margin-bottom: -50px; }\n\n.card.hovercard .avatar img {\n  width: 100px;\n  height: 100px;\n  max-width: 100px;\n  max-height: 100px;\n  border-radius: 50%;\n  border: 5px solid rgba(255, 255, 255, 0.5); }\n\n.card.hovercard .info .title {\n  margin-bottom: 4px;\n  font-size: 24px;\n  line-height: 1;\n  color: #262626;\n  vertical-align: middle; }\n\n.card.hovercard .info .desc {\n  overflow: hidden;\n  font-size: 12px;\n  line-height: 20px;\n  color: #737373;\n  text-overflow: ellipsis; }\n\n.card.hovercard .bottom {\n  padding: 0 20px;\n  margin-bottom: 17px; }\n\n.btn {\n  border-radius: 50%;\n  width: 32px;\n  height: 32px;\n  line-height: 18px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2JhdmFuaS9EZXNrdG9wL3dvcmtzcGFjZS9kcml2ZXJhcHAvc3JjL2FwcC9zZXR0aW5ncy9zZXR0aW5ncy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVTtFQUNWLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFHbEI7RUFDSSxjQUFhO0VBQ2Isd0JBQXdCLEVBQUE7O0FBR3hCOztFQUVJLFdBQVU7RUFDVixtQkFBa0I7RUFDbEIsc0JBQXFCLEVBQUE7O0FBR3JCO0VBQ0ksaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUdwQjtFQUNJLG1CQUFtQjtFQUNuQixVQUFVLEVBQUE7O0FBR2Q7RUFDSSxVQUFVLEVBQUE7O0FBR2Q7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHeEI7RUFDSSxpQkFBaUI7RUFDakIsb0JBQW9CLEVBQUE7O0FBR3BDO0VBQ0k7SUFDSSxjQUFhO0lBQ2IsV0FBVyxFQUFBO0VBR1g7O0lBRUksV0FBVTtJQUNWLGNBQWE7SUFDYixzQkFBcUI7SUFDckIsa0JBQWtCLEVBQUE7RUFHdEI7SUFDSSxrQkFBa0IsRUFBQSxFQUNyQjs7QUFLVDtFQUNJLFdBQVc7RUFDWCxzQkFBc0I7RUFDdEIsa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksV0FBVztFQUNYLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFHekI7RUFDSSxXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLHFCQUFxQixFQUFBOztBQUd6QjtFQUNJLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIscUJBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksV0FBVztFQUNYLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFHekI7RUFDSSxXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLHFCQUFxQixFQUFBOztBQUd6Qjs7Ozs7RUFLSSxrQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxVQUFVO0VBQ1YsV0FBVztFQUNYLG1CQUFtQixFQUFBOztBQUd2QjtFQUVJLDJCQUEyQjtFQUMzQixVQUFVO0VBQ1YsVUFBVSxFQUFBOztBQUdkO0VBR0E7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBYyxFQUFBO0VBR2xCO0lBRUksMkJBQTJCO0lBQzNCLFVBQVU7SUFDVixVQUFVLEVBQUEsRUFDYjs7QUFHRDtFQUNJLDBDQUEwQztFQUMxQyxtQkFBbUI7RUFDbkIsd0JBQXdCO0VBR3hCLGtCQUFrQjtFQUdsQixnQkFBZ0I7RUFHaEIsc0JBQXNCLEVBQUE7O0FBRzFCO0VBQ0ksZUFBZTtFQUNmLFNBQVMsRUFBQTs7QUFHYjtFQUNJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLGdDQUFnQyxFQUFBOztBQUdwQztFQUNJLHFCQUFxQjtFQUNyQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsU0FBUztFQUdULGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLHFCQUFxQjtFQUNyQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxTQUFTO0VBQ1QsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixjQUFjLEVBQUE7O0FBR2xCO0VBQ0ksZUFBZTtFQUNmLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksZUFBZTtFQUNmLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksZ0JBQWdCO0VBQ2hCLHlCQUF5QjtFQUN6QixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxhQUFhO0VBQ2IsU0FBUztFQUNULHlCQUF5QixFQUFBOztBQUc3QjtFQUNJLFVBQVU7RUFDVix3QkFBd0IsRUFBQTs7QUFHNUI7O0VBRUksa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsV0FBVztFQUNYLHVCQUF1QjtFQUN2QixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxlQUFlO0VBQ2YsaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osYUFBYTtFQUNiLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLE9BQU87RUFDUCxxQkFBcUI7RUFDckIsWUFBWTtFQUNaLGFBQWE7RUFDYix5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLHFCQUFxQjtFQUNyQixXQUFXO0VBQ1gsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFHbkIsc0JBQXNCLEVBQUE7O0FBRzFCO0VBQ0ksY0FBYztFQUNkLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsY0FBYyxFQUFBOztBQUdsQjtFQUNJLGNBQWM7RUFDZCx1QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYztFQUNkLHVCQUF1QixFQUFBOztBQUczQjtFQUNJLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsT0FBTztFQUNQLHFCQUFxQjtFQUNyQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFHbEIsc0JBQXNCLEVBQUE7O0FBRzFCO0VBQ0ksa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLDBDQUEwQyxFQUFBOztBQUc5QztFQUNJLHFHQUFxRztFQUNyRyxzQkFBc0I7RUFDdEIsYUFBYSxFQUFBOztBQUdqQjtFQUNJLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1Ysb0JBQW9CLEVBQUE7O0FBR3hCO0VBQ0ksWUFBWTtFQUNaLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBR2pCLGtCQUFrQjtFQUNsQiwwQ0FBdUMsRUFBQTs7QUFLM0M7RUFDSSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsc0JBQXNCLEVBQUE7O0FBRzFCO0VBQ0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYztFQUNkLHVCQUF1QixFQUFBOztBQUczQjtFQUNJLGVBQWU7RUFDZixtQkFBbUIsRUFBQTs7QUFHdkI7RUFBTSxrQkFBa0I7RUFBRSxXQUFVO0VBQUUsWUFBVztFQUFFLGlCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2V0dGluZ3Mvc2V0dGluZ3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJzLWNhbGx0b2FjdGlvbntcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6YXV0bztcbiAgICBwYWRkaW5nOiAxNXB4IDI1cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuICAgIC5icy1jYWxsdG9hY3Rpb24gPiAucm93e1xuICAgICAgICBkaXNwbGF5OnRhYmxlO1xuICAgICAgICB3aWR0aDogY2FsYygxMDAlICsgMzBweCk7XG4gICAgfVxuICAgICBcbiAgICAgICAgLmJzLWNhbGx0b2FjdGlvbiA+IC5yb3cgPiBbY2xhc3NePVwiY29sLVwiXSxcbiAgICAgICAgLmJzLWNhbGx0b2FjdGlvbiA+IC5yb3cgPiBbY2xhc3MqPVwiIGNvbC1cIl17XG4gICAgICAgICAgICBmbG9hdDpub25lO1xuICAgICAgICAgICAgZGlzcGxheTp0YWJsZS1jZWxsO1xuICAgICAgICAgICAgdmVydGljYWwtYWxpZ246bWlkZGxlO1xuICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jdGEtY29udGVudHN7XG4gICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAuY3RhLXRpdGxle1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDAgYXV0byAxNXB4O1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC5jdGEtZGVzY3tcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAuY3RhLWRlc2MgcDpsYXN0LWNoaWxke1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmN0YS1idXR0b257XG4gICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICB9XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA5OTFweCl7XG4gICAgLmJzLWNhbGx0b2FjdGlvbiA+IC5yb3d7XG4gICAgICAgIGRpc3BsYXk6YmxvY2s7XG4gICAgICAgIHdpZHRoOiBhdXRvO1xuICAgIH1cblxuICAgICAgICAuYnMtY2FsbHRvYWN0aW9uID4gLnJvdyA+IFtjbGFzc149XCJjb2wtXCJdLFxuICAgICAgICAuYnMtY2FsbHRvYWN0aW9uID4gLnJvdyA+IFtjbGFzcyo9XCIgY29sLVwiXXtcbiAgICAgICAgICAgIGZsb2F0Om5vbmU7XG4gICAgICAgICAgICBkaXNwbGF5OmJsb2NrO1xuICAgICAgICAgICAgdmVydGljYWwtYWxpZ246bWlkZGxlO1xuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICB9XG5cbiAgICAgICAgLmN0YS1jb250ZW50c3tcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgfVxufVxuXG5cblxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24tZGVmYXVsdHtcbiAgICBjb2xvcjogIzMzMztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgIGJvcmRlci1jb2xvcjogI2NjYztcbn1cblxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24tcHJpbWFyeXtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzM3YWI3O1xuICAgIGJvcmRlci1jb2xvcjogIzJlNmRhNDtcbn1cblxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24taW5mb3tcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWJjMGRlO1xuICAgIGJvcmRlci1jb2xvcjogIzQ2YjhkYTtcbn1cblxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24tc3VjY2Vzc3tcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xuICAgIGJvcmRlci1jb2xvcjogIzRjYWU0Yztcbn1cblxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24td2FybmluZ3tcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBhZDRlO1xuICAgIGJvcmRlci1jb2xvcjogI2VlYTIzNjtcbn1cblxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24tZGFuZ2Vye1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkOTUzNGY7XG4gICAgYm9yZGVyLWNvbG9yOiAjZDQzZjNhO1xufVxuXG4uYnMtY2FsbHRvYWN0aW9uLmJzLWNhbGx0b2FjdGlvbi1wcmltYXJ5IC5jdGEtYnV0dG9uIC5idG4sXG4uYnMtY2FsbHRvYWN0aW9uLmJzLWNhbGx0b2FjdGlvbi1pbmZvIC5jdGEtYnV0dG9uIC5idG4sXG4uYnMtY2FsbHRvYWN0aW9uLmJzLWNhbGx0b2FjdGlvbi1zdWNjZXNzIC5jdGEtYnV0dG9uIC5idG4sXG4uYnMtY2FsbHRvYWN0aW9uLmJzLWNhbGx0b2FjdGlvbi13YXJuaW5nIC5jdGEtYnV0dG9uIC5idG4sXG4uYnMtY2FsbHRvYWN0aW9uLmJzLWNhbGx0b2FjdGlvbi1kYW5nZXIgLmN0YS1idXR0b24gLmJ0bntcbiAgICBib3JkZXItY29sb3I6I2ZmZjtcbn1cblxuLmZiLXByb2ZpbGUgaW1nLmZiLWltYWdlLWxne1xuICAgIHotaW5kZXg6IDA7XG4gICAgd2lkdGg6IDEwMCU7ICBcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4uZmItaW1hZ2UtcHJvZmlsZVxue1xuICAgIG1hcmdpbjogLTkwcHggMTBweCAwcHggNTBweDtcbiAgICB6LWluZGV4OiA5O1xuICAgIHdpZHRoOiAyMCU7IFxufVxuXG5AbWVkaWEgKG1heC13aWR0aDo3NjhweClcbntcbiAgICBcbi5mYi1wcm9maWxlLXRleHQ+aDF7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBmb250LXNpemU6MTZweDtcbn1cblxuLmZiLWltYWdlLXByb2ZpbGVcbntcbiAgICBtYXJnaW46IC00NXB4IDEwcHggMHB4IDI1cHg7XG4gICAgei1pbmRleDogOTtcbiAgICB3aWR0aDogMjAlOyBcbn1cbn1cblxuLmNhcmQge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjE0LCAyMjQsIDIyNiwgMC4yKTtcbiAgICBib3JkZXItdG9wLXdpZHRoOiAwO1xuICAgIGJvcmRlci1ib3R0b20td2lkdGg6IDJweDtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xuICAgIC1tb3otYm94LXNoYWRvdzogbm9uZTtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAtbW96LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbn1cblxuLmNhcmQgLmNhcmQtaGVhZGluZyB7XG4gICAgcGFkZGluZzogMCAyMHB4O1xuICAgIG1hcmdpbjogMDtcbn1cblxuLmNhcmQgLmNhcmQtaGVhZGluZy5zaW1wbGUge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBmb250LXdlaWdodDogMzAwO1xuICAgIGNvbG9yOiAjNzc3O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTVlNWU1O1xufVxuXG4uY2FyZCAuY2FyZC1oZWFkaW5nLmltYWdlIGltZyB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA0NnB4O1xuICAgIGhlaWdodDogNDZweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICBib3JkZXI6IDA7XG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xufVxuXG4uY2FyZCAuY2FyZC1oZWFkaW5nLmltYWdlIC5jYXJkLWhlYWRpbmctaGVhZGVyIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbn1cblxuLmNhcmQgLmNhcmQtaGVhZGluZy5pbWFnZSAuY2FyZC1oZWFkaW5nLWhlYWRlciBoMyB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBjb2xvcjogIzI2MjYyNjtcbn1cblxuLmNhcmQgLmNhcmQtaGVhZGluZy5pbWFnZSAuY2FyZC1oZWFkaW5nLWhlYWRlciBzcGFuIHtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgY29sb3I6ICM5OTk5OTk7XG59XG5cbi5jYXJkIC5jYXJkLWJvZHkge1xuICAgIHBhZGRpbmc6IDAgMjBweDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4uY2FyZCAuY2FyZC1tZWRpYSB7XG4gICAgcGFkZGluZzogMCAyMHB4O1xuICAgIG1hcmdpbjogMCAtMTRweDtcbn1cblxuLmNhcmQgLmNhcmQtbWVkaWEgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgbWF4LWhlaWdodDogMTAwJTtcbn1cblxuLmNhcmQgLmNhcmQtYWN0aW9ucyB7XG4gICAgbWluLWhlaWdodDogMzBweDtcbiAgICBwYWRkaW5nOiAwIDIwcHggMjBweCAyMHB4O1xuICAgIG1hcmdpbjogMjBweCAwIDAgMDtcbn1cblxuLmNhcmQgLmNhcmQtY29tbWVudHMge1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgbWFyZ2luOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOGY4Zjg7XG59XG5cbi5jYXJkIC5jYXJkLWNvbW1lbnRzIC5jb21tZW50cy1jb2xsYXBzZS10b2dnbGUge1xuICAgIHBhZGRpbmc6IDA7XG4gICAgbWFyZ2luOiAwIDIwcHggMTJweCAyMHB4O1xufVxuXG4uY2FyZCAuY2FyZC1jb21tZW50cyAuY29tbWVudHMtY29sbGFwc2UtdG9nZ2xlIGEsXG4uY2FyZCAuY2FyZC1jb21tZW50cyAuY29tbWVudHMtY29sbGFwc2UtdG9nZ2xlIHNwYW4ge1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBjb2xvcjogIzk5OTtcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xufVxuXG4uY2FyZC1jb21tZW50cyAubWVkaWEtaGVhZGluZyB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY2FyZC5wZW9wbGUge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDE3MHB4O1xuICAgIGhlaWdodDogMzAwcHg7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xufVxuXG4uY2FyZC5wZW9wbGU6Zmlyc3QtY2hpbGQge1xuICAgIG1hcmdpbi1sZWZ0OiAwO1xufVxuXG4uY2FyZC5wZW9wbGUgLmNhcmQtdG9wIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxNzBweDtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG5cbi5jYXJkLnBlb3BsZSAuY2FyZC10b3AuZ3JlZW4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM1M2E5M2Y7XG59XG5cbi5jYXJkLnBlb3BsZSAuY2FyZC10b3AuYmx1ZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzQyN2ZlZDtcbn1cblxuLmNhcmQucGVvcGxlIC5jYXJkLWluZm8ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDE1MHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMXB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbi5jYXJkLnBlb3BsZSAuY2FyZC1pbmZvIC50aXRsZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luOiA4cHggMTRweCAwIDE0cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgY29sb3I6ICM0MDQwNDA7XG59XG5cbi5jYXJkLnBlb3BsZSAuY2FyZC1pbmZvIC5kZXNjIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW46IDhweCAxNHB4IDAgMTRweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBjb2xvcjogIzczNzM3MztcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cblxuLmNhcmQucGVvcGxlIC5jYXJkLWJvdHRvbSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMDtcbiAgICBsZWZ0OiAwO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDI5cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAtbW96LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbn1cblxuLmNhcmQuaG92ZXJjYXJkIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyMTQsIDIyNCwgMjI2LCAwLjIpO1xufVxuXG4uY2FyZC5ob3ZlcmNhcmQgLmNhcmRoZWFkZXIge1xuICAgIGJhY2tncm91bmQ6IHVybChcImh0dHBzOi8vYXBpLmJvb2Rza2FwLmlvL2ZpbGVzL3B1YmxpYy9kb3dubG9hZC80NzEyMzNjMS03OTMzLTQ5M2YtOTg1Mi00YzVhYzZhYTEzNTJcIik7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBoZWlnaHQ6IDIwMHB4O1xufVxuXG4uY2FyZC5ob3ZlcmNhcmQgLmF2YXRhciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogLTUwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTUwcHg7XG59XG5cbi5jYXJkLmhvdmVyY2FyZCAuYXZhdGFyIGltZyB7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgbWF4LXdpZHRoOiAxMDBweDtcbiAgICBtYXgtaGVpZ2h0OiAxMDBweDtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYm9yZGVyOiA1cHggc29saWQgcmdiYSgyNTUsMjU1LDI1NSwwLjUpO1xufVxuXG5cblxuLmNhcmQuaG92ZXJjYXJkIC5pbmZvIC50aXRsZSB7XG4gICAgbWFyZ2luLWJvdHRvbTogNHB4O1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICBsaW5lLWhlaWdodDogMTtcbiAgICBjb2xvcjogIzI2MjYyNjtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuXG4uY2FyZC5ob3ZlcmNhcmQgLmluZm8gLmRlc2Mge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIGNvbG9yOiAjNzM3MzczO1xuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG4uY2FyZC5ob3ZlcmNhcmQgLmJvdHRvbSB7XG4gICAgcGFkZGluZzogMCAyMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDE3cHg7XG59XG5cbi5idG57IGJvcmRlci1yYWRpdXM6IDUwJTsgd2lkdGg6MzJweDsgaGVpZ2h0OjMycHg7IGxpbmUtaGVpZ2h0OjE4cHg7ICB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/settings/settings.page.ts":
/*!*******************************************!*\
  !*** ./src/app/settings/settings.page.ts ***!
  \*******************************************/
/*! exports provided: SettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPage", function() { return SettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_device_motion_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/device-motion/ngx */ "./node_modules/@ionic-native/device-motion/ngx/index.js");










var SettingsPage = /** @class */ (function () {
    function SettingsPage(menu, navCtrl, platform, commonService, router, loadingController, storage, formBuilder, datePipe, geolocation, deviceMotion) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.commonService = commonService;
        this.router = router;
        this.loadingController = loadingController;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.datePipe = datePipe;
        this.geolocation = geolocation;
        this.deviceMotion = deviceMotion;
        this.openMenu = false;
        this.showLiveUpdatePopup = false;
        this.menu.enable(true);
    }
    SettingsPage.prototype.searchFilter = function () {
        this.togglePopupMenu();
    };
    SettingsPage.prototype.togglePopupMenu = function () {
        if (!this.showLiveUpdatePopup) {
            return this.openMenu = !this.openMenu;
        }
    };
    SettingsPage.prototype.navigateTo = function (page) {
        this.router.navigate(['/' + page]);
    };
    SettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! ./settings.page.html */ "./src/app/settings/settings.page.html"),
            styles: [__webpack_require__(/*! ./settings.page.scss */ "./src/app/settings/settings.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"], _ionic_native_device_motion_ngx__WEBPACK_IMPORTED_MODULE_9__["DeviceMotion"]])
    ], SettingsPage);
    return SettingsPage;
}());



/***/ })

}]);
//# sourceMappingURL=settings-settings-module.js.map