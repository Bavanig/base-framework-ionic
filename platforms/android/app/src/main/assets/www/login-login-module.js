(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.html":
/*!***************************************!*\
  !*** ./src/app/login/login.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-content>\n  <div id=\"loginPage\">\n  <div class=\"padded\">\n\n    <div class=\"login-form\">\n\n        <div class=\"logo-box\">\n            <img src=\"/assets/icon/ionic_base.png\" style=\"height: 72px;\">\n        </div>\n\n        <form   [formGroup]=\"loginForm\" id=\"myform\" style=\"width: 80%;margin:0 auto;\">\n\n            <div id=\"triangle\"></div>\n\n            <div class=\"login-box\">\n                <input type=\"text\" class=\"text-input text-input--material\" placeholder=\"Username\" id=\"username\"  formControlName=\"userName\" >\n\n                <div style=\"position: relative;\">\n                    <input [type]=\"type\"  class=\"text-input text-input--material\" id=\"loginpwd\" formControlName=\"password\" placeholder=\"Password\" required>\n                    <button class=\"pwd-align\" type=\"button\" >\n                        <i class=\"fa fa-eye\" *ngIf=\"eyeOn\" (click)=\"eyeOn = false; type = 'password'\"></i>\n                        <i class=\"fa fa-eye-slash\" *ngIf=\"!eyeOn\" (click)=\"eyeOn = true; type = 'text'\"></i>\n                    </button>\n                </div>\n\n                <br>\n                <ion-button class=\"login-button\" [disabled]=\"loginForm.invalid\" (click)=\"login()\">\n                    <span id=\"loginTxt\" class=\"login-fdbk\" style=\"font-family: Play;\">Log In</span>\n                    <span id=\"loginFdbk\" class=\"login-fdbk\" style=\"display: none;\">\n                   <i class=\"fa fa-circle-o-notch fa-spin\"></i>\n                   <span style=\"font-family: Play;\">Loading...</span>\n                  </span>\n                </ion-button>\n                <br>\n            </div>\n            <!-- <div><span style=\"font-size: 12px;color: #666666;font-family: Play;\">V <span class=\"app-version\">1.0.0</span></span></div> -->\n\n            <!-- <div><span style=\"font-size: 12px;color: #666666;font-family: Play;\">V <span class=\"app-version\">{{ appVersion }}</span></span></div> -->\n\n            <p class=\"p-by\">\n                <a href=\"javascript:void(0)\" style=\"color: #666666;font-size: 12px;text-decoration: none;\">\n                    Powered by <img src=\"/assets/icon/boodskap-logo.png\" style=\"height: 25px;vertical-align: -8px;\">  Boodskap\n                </a>\n            </p>\n          \n        </form>\n\n    </div>\n\n</div>\n</div>\n</ion-content>   \n"

/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".p-by {\n  font-family: Play;\n  background: rgba(255, 255, 255, 0.18);\n  margin-top: 20%;\n  display: inline-block;\n  padding: 0 10px;\n  border-radius: 5px;\n  font-size: 10px; }\n\n#loginPage .page__background {\n  background: #ffffff;\n  /*opacity: 0.7;*/\n  /*background-image: url('img/amb.jpg') !important;*/\n  /*-webkit-background-size: cover;*/\n  /*-moz-background-size: center cover;*/\n  /*background-size: cover;*/\n  /*background-position: center top;*/ }\n\n#loginPage .login-form {\n  text-align: center;\n  margin: 60px auto 0;\n  margin-top: 10%;\n  width: 100%;\n  background: none;\n  padding: 25px 0; }\n\n#loginPage #username, #loginpwd {\n  display: block;\n  width: 100%;\n  margin: 0 auto;\n  outline: none;\n  height: 100%;\n  padding: 15px 20px;\n  color: #333333;\n  font-size: 16px;\n  background: #eeeeee;\n  margin-top: 7px;\n  border: 0;\n  border-bottom: 1px solid #cccccc;\n  border-radius: 0; }\n\n#loginPage #username::-webkit-input-placeholder, #loginpwd::-webkit-input-placeholder {\n  color: #999999; }\n\n#loginPage #username:-ms-input-placeholder, #loginpwd:-ms-input-placeholder {\n  color: #999999; }\n\n#loginPage #username::-ms-input-placeholder, #loginpwd::-ms-input-placeholder {\n  color: #999999; }\n\n#loginPage #username::placeholder, #loginpwd::placeholder {\n  color: #999999; }\n\n#loginPage .login-button {\n  width: 100%;\n  margin: 0 auto;\n  border-radius: 5px;\n  background: #397ffd;\n  color: #ffffff;\n  font-weight: 600; }\n\n#loginPage .big-title {\n  font-size: 12px;\n  margin: 0;\n  width: 130px;\n  display: inline-block;\n  padding: 5px 15px;\n  background: rgba(255, 255, 255, 0.18);\n  border-radius: 10px;\n  margin-bottom: 20px; }\n\n#loginPage .forgot-password {\n  display: block;\n  margin: 8px auto 0 auto;\n  font-size: 14px;\n  color: #397ffd; }\n\n#loginPage .pwd-align {\n  position: absolute;\n  right: 6px;\n  bottom: 15px;\n  background: transparent;\n  border: 0;\n  font-size: 20px;\n  outline: none; }\n\n#loginPage .slide {\n  width: 90%;\n  margin: 0 auto; }\n\n#loginPage .pwd-align i {\n  color: #666666; }\n\n#triangle {\n  width: 0;\n  height: 0;\n  border-bottom: 23px solid #fff;\n  border-left: 25px solid transparent;\n  border-right: 25px solid transparent;\n  /* text-align: center; */\n  margin: 0 auto; }\n\n.logo-box {\n  width: 65%;\n  display: inline-block;\n  background: #ffffff;\n  padding: 7px;\n  border-radius: 8px;\n  margin-top: 1em;\n  margin-bottom: 1em; }\n\n.app-name {\n  font-family: 'Play';\n  color: #666666;\n  font-size: 20px; }\n\n.login-box {\n  background: #ffffff;\n  border: 1px solid #ffffff;\n  border-radius: 3px;\n  padding: 10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2JhdmFuaS9EZXNrdG9wL3dvcmtzcGFjZS9kcml2ZXJhcHAvc3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7RUFDakIscUNBQWtDO0VBQ2xDLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBR2pCO0VBQ0UsbUJBQW1CO0VBQ25CLGdCQUFBO0VBQ0EsbURBQUE7RUFDQSxrQ0FBQTtFQUNBLHNDQUFBO0VBQ0EsMEJBQUE7RUFDQSxtQ0FBQSxFQUFvQzs7QUFHdEM7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWM7RUFDZCxXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHakI7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLGNBQWM7RUFDZCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsU0FBUztFQUNULGdDQUFnQztFQUNoQyxnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxjQUFjLEVBQUE7O0FBRGhCO0VBQ0UsY0FBYyxFQUFBOztBQURoQjtFQUNFLGNBQWMsRUFBQTs7QUFEaEI7RUFDRSxjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsV0FBVztFQUNYLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxlQUFlO0VBQ2YsU0FBUztFQUNULFlBQVk7RUFDWixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLHFDQUFrQztFQUNsQyxtQkFBbUI7RUFDbkIsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsY0FBYztFQUNkLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2YsY0FBYyxFQUFBOztBQUVoQjtFQUNFLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsWUFBWTtFQUNaLHVCQUF1QjtFQUN2QixTQUFTO0VBQ1QsZUFBZTtFQUNmLGFBQWEsRUFBQTs7QUFFZjtFQUNFLFVBQVU7RUFDVixjQUFjLEVBQUE7O0FBRWhCO0VBQ0UsY0FBYyxFQUFBOztBQUVoQjtFQUNFLFFBQVE7RUFDUixTQUFTO0VBQ1QsOEJBQThCO0VBQzlCLG1DQUFtQztFQUNuQyxvQ0FBb0M7RUFDcEMsd0JBQUE7RUFDQSxjQUFjLEVBQUE7O0FBRWhCO0VBQ0UsVUFBVTtFQUNWLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxlQUFlLEVBQUE7O0FBRWpCO0VBQ0UsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixrQkFBa0I7RUFDbEIsYUFBYSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnAtYnl7XG4gIGZvbnQtZmFtaWx5OiBQbGF5O1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuMTgpO1xuICBtYXJnaW4tdG9wOiAyMCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogMCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuI2xvZ2luUGFnZSAucGFnZV9fYmFja2dyb3VuZCB7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIC8qb3BhY2l0eTogMC43OyovXG4gIC8qYmFja2dyb3VuZC1pbWFnZTogdXJsKCdpbWcvYW1iLmpwZycpICFpbXBvcnRhbnQ7Ki9cbiAgLyotd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7Ki9cbiAgLyotbW96LWJhY2tncm91bmQtc2l6ZTogY2VudGVyIGNvdmVyOyovXG4gIC8qYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsqL1xuICAvKmJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciB0b3A7Ki9cbn1cblxuI2xvZ2luUGFnZSAubG9naW4tZm9ybSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiA2MHB4IGF1dG8gMDtcbiAgbWFyZ2luLXRvcDoxMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBwYWRkaW5nOiAyNXB4IDA7XG59XG5cbiNsb2dpblBhZ2UgI3VzZXJuYW1lLCAjbG9naW5wd2Qge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBvdXRsaW5lOiBub25lO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBhZGRpbmc6IDE1cHggMjBweDtcbiAgY29sb3I6ICMzMzMzMzM7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgYmFja2dyb3VuZDogI2VlZWVlZTtcbiAgbWFyZ2luLXRvcDogN3B4O1xuICBib3JkZXI6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjY2NjO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuXG4jbG9naW5QYWdlICN1c2VybmFtZTo6cGxhY2Vob2xkZXIsICNsb2dpbnB3ZDo6cGxhY2Vob2xkZXIge1xuICBjb2xvcjogIzk5OTk5OTtcbn1cblxuI2xvZ2luUGFnZSAubG9naW4tYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJhY2tncm91bmQ6ICMzOTdmZmQ7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuI2xvZ2luUGFnZSAuYmlnLXRpdGxle1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbjogMDtcbiAgd2lkdGg6IDEzMHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuMTgpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuI2xvZ2luUGFnZSAuZm9yZ290LXBhc3N3b3JkIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogOHB4IGF1dG8gMCBhdXRvO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiAjMzk3ZmZkO1xufVxuI2xvZ2luUGFnZSAucHdkLWFsaWduIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogNnB4O1xuICBib3R0b206IDE1cHg7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDA7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgb3V0bGluZTogbm9uZTtcbn1cbiNsb2dpblBhZ2UgLnNsaWRle1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW46IDAgYXV0bztcbn1cbiNsb2dpblBhZ2UgLnB3ZC1hbGlnbiBpe1xuICBjb2xvcjogIzY2NjY2Njtcbn1cbiN0cmlhbmdsZSB7XG4gIHdpZHRoOiAwO1xuICBoZWlnaHQ6IDA7XG4gIGJvcmRlci1ib3R0b206IDIzcHggc29saWQgI2ZmZjtcbiAgYm9yZGVyLWxlZnQ6IDI1cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIGJvcmRlci1yaWdodDogMjVweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgLyogdGV4dC1hbGlnbjogY2VudGVyOyAqL1xuICBtYXJnaW46IDAgYXV0bztcbn1cbi5sb2dvLWJveHtcbiAgd2lkdGg6IDY1JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA3cHg7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgbWFyZ2luLXRvcDogMWVtO1xuICBtYXJnaW4tYm90dG9tOiAxZW07XG59XG4uYXBwLW5hbWV7XG4gIGZvbnQtZmFtaWx5OiAnUGxheSc7XG4gIGNvbG9yOiAjNjY2NjY2O1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ubG9naW4tYm94e1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmZmZmZmO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/fcm/ngx */ "./node_modules/@ionic-native/fcm/ngx/index.js");









var LoginPage = /** @class */ (function () {
    function LoginPage(menu, formBuilder, loadingController, commonService, router, storage, fcm, alertController) {
        var _this = this;
        this.menu = menu;
        this.formBuilder = formBuilder;
        this.loadingController = loadingController;
        this.commonService = commonService;
        this.router = router;
        this.storage = storage;
        this.fcm = fcm;
        this.alertController = alertController;
        this.appVersion = '1.1.4';
        this.eyeOn = false;
        this.type = 'password';
        this.loginForm = this.formBuilder.group({
            userName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
        });
        this.storage.get('userDetails').then(function (val) {
            console.log('userDetails =', val);
            if (val !== undefined && val !== null) {
                _this.router.navigate(['/dashboard']);
            }
        });
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.ionViewWillEnter = function () {
        this.menu.enable(false);
    };
    LoginPage.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, actionURL;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'Loading...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        actionURL = "domain/login/" + this.loginForm.value.userName + "/" + this.loginForm.value.password + '?targetDomainKey=' + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].DOMAIN_KEY;
                        return [4 /*yield*/, this.commonService.getAllCall(actionURL)
                                .subscribe(function (res) {
                                console.log(res);
                                if (res) {
                                    var actionUrl = 'sql/exec/multi/' + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_TOKEN;
                                    var data = {
                                        "queries": [
                                            { "type": "SELECT", "query": "SELECT * FROM " + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].DRIVER_MASTER + " WHERE email='" + _this.loginForm.value.userName + "'" },
                                        ]
                                    };
                                    _this.commonService.postCall(actionUrl, JSON.stringify(data))
                                        .subscribe(function (result) {
                                        res['driverObj'] = result.results[0].records[0];
                                        res['driverObj']['driverimg'] = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].fileUrl + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_TOKEN + '/' + res['driverObj']['driverimg'];
                                        _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_KEY = res.apiKey;
                                        _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_TOKEN = res.token;
                                        _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].DOMAIN_KEY = res.domainKey;
                                        _this.storage.set('userDetails', res);
                                        /////////////////// FCM //////////////////////
                                        _this.fcm.subscribeToTopic('drivermodule');
                                        _this.fcm.getToken().then(function (token) {
                                            var path = "fcm/register/" + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_TOKEN + "/" + res['driverObj'].email + "/" + device.model + "/" + device.version + "/" + token;
                                            _this.commonService.getAllCall(path)
                                                .subscribe(function (res) {
                                                _this.commonService.presentToast('Device ID registered successfully');
                                            });
                                        });
                                        _this.fcm.onNotification().subscribe(function (data) {
                                            _this.presentAlertConfirm('You got new trip!');
                                        });
                                        /////////////////// FCM //////////////////////
                                        _this.router.navigate(['/dashboard']);
                                    });
                                }
                                else {
                                    _this.commonService.presentToast('Username/Password Invalid');
                                }
                                loading.dismiss();
                            }, function (err) {
                                console.log(err);
                                loading.dismiss();
                            })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.presentAlertConfirm = function (status) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'New Trip Alert',
                            message: status,
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        _this.commonService.presentToast('Your option have been saved.');
                                    }
                                }, {
                                    text: 'Okay',
                                    handler: function () {
                                        _this.router.navigate(['/home']);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__["FCM"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map